﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Threading;

namespace PortScanSample
{
    class Program
    {
        static void Main(string[] args)
        {
            TimerCallback tmCallback = CheckEffectExpiry;
            Timer timer = new Timer(tmCallback, "usb", 1000, 10000);
            Console.Read();
        }

        static void CheckEffectExpiry(object objectInfo)
        {
            Console.Clear();
            var usbDevices = GetUSBDevices();
            int i = 1;
            Console.WriteLine("Following are the devices connected to this system");
            Console.WriteLine(Environment.NewLine);
            foreach (var usbDevice in usbDevices)
            {
                Console.WriteLine("Device " + i);
                Console.WriteLine("Device ID: {0}", usbDevice.DeviceID);
                Console.WriteLine("PNP Device ID: {0}", usbDevice.PnpDeviceID);
                Console.WriteLine("Description: {0}", usbDevice.Description);
                Console.WriteLine("------------------------------------------------------------------------------");
                i++;
            }
        }
        
        static List<USBDeviceInfo> GetUSBDevices()
        {
            List<USBDeviceInfo> devices = new List<USBDeviceInfo>();

            ManagementObjectCollection collection;
            using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_PnPEntity where DeviceID Like ""USB%"""))
                collection = searcher.Get();

            foreach (var device in collection)
            {
                devices.Add(new USBDeviceInfo(
                (string)device.GetPropertyValue("DeviceID"),
                (string)device.GetPropertyValue("PNPDeviceID"),
                (string)device.GetPropertyValue("Description")
                ));
            }

            collection.Dispose();
            return devices;
        }
    }

    class USBDeviceInfo
    {
        public USBDeviceInfo(string deviceID, string pnpDeviceID, string description)
        {
            this.DeviceID = deviceID;
            this.PnpDeviceID = pnpDeviceID;
            this.Description = description;
        }
        public string DeviceID { get; private set; }
        public string PnpDeviceID { get; private set; }
        public string Description { get; private set; }
    }
}
